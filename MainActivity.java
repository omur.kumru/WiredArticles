package com.ok.wiredarticles;


import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<ArrayList<Article>> {

    TextView mTextView;
    LoaderManager mLoaderManager;
    ProgressBar progressBar;

    CustomAdapter customAdapter;
    ArrayList<Article> articleList;

    private void init() {

        customAdapter = new CustomAdapter(this, new ArrayList<Article>());
        articleList = new ArrayList<>();
        progressBar = (ProgressBar) findViewById(R.id.progressBarMain);
        progressBar = (ProgressBar) findViewById(R.id.progressBarMain);
        ListView listView = (ListView) findViewById(R.id.liste);
        listView.setAdapter(customAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Article article = articleList.get(position);
                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                intent.putExtra("ArticleLink", article.getLink());
                intent.putExtra("ArticleHeadLine", article.getArticleHeadline());
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                finish();
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        boolean isConnected=ConnectionCheck.isNetworkAvailable(this);

        if (isConnected) {
            mLoaderManager = getLoaderManager();
            mLoaderManager.initLoader(1, null, this);
            init();
        }else{
            Toast.makeText(this,"Network Error",Toast.LENGTH_LONG).show();
        }


    }




    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
    }


    @Override
    public Loader<ArrayList<Article>> onCreateLoader(int id, Bundle args) {


        return new ArticleAsyncTaskLoader(this);
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<Article>> loader, ArrayList<Article> data) {

        progressBar.setVisibility(View.GONE);
        Log.d("sdasd", data.get(1).getDate());
        customAdapter.addAll(data);
        articleList = data;
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<Article>> loader) {

    }


}
