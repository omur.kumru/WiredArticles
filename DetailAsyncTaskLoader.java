package com.ok.wiredarticles;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Cleaner;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ok on 7.05.2017.
 */

public class DetailAsyncTaskLoader extends AsyncTaskLoader<StringAndList> {


    private String linkArticle;



    public DetailAsyncTaskLoader(Context context, String link) {
        super(context);
        linkArticle=link;

    }

    @Override
    public StringAndList loadInBackground() {

        Document doc = null;



        try {
            doc = Jsoup.connect(linkArticle).get();//Site ile bağlantı kurulur. Bu bağlantı doc nesnesine aktarılır.
        } catch (IOException e) {
            e.printStackTrace();
        }

        doc.select("a[href]").unwrap();
        doc.select(".gray-5").remove();
        doc.select(".span").remove();





        Elements element = doc.select("#start-of-content").select("p");


        String completeArticle= element.toString().replaceAll("&nbsp;"," ").replaceAll("<p>","").replaceAll("</p>","\n").replaceAll("<em>","").replaceAll("</em>","");


        String string2=Jsoup.clean(completeArticle, "", Whitelist.none(), new Document.OutputSettings().prettyPrint(false));

        List<String> fiveWords=RepeatedWords.repeatedWords(string2);

        HashMap<String,String> fiveWordsWithMeaning=JsonRequest.getJsonMeaning(fiveWords);

        StringAndList snl= new StringAndList(string2,fiveWordsWithMeaning);




        return snl;


    }

    @Override
    protected void onStartLoading () {
        forceLoad();
    }
}
