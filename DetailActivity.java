package com.ok.wiredarticles;

import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class DetailActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks {

    String headLine;
    String link;
    String article;
    LoaderManager mLoaderManager;
    TextView textArticle;
    TextView englishword1,englishword2,englishword3,englishword4,englishword5;
    TextView turkishword1,turkishword2,turkishword3,turkishword4,turkishword5;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        boolean isConnected=ConnectionCheck.isNetworkAvailable(this);

        if (isConnected) {
        initViews();
        mLoaderManager = getLoaderManager();
        mLoaderManager.initLoader(1, null, this);
        }else{
            Toast.makeText(this,"Network Error",Toast.LENGTH_LONG).show();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(DetailActivity.this, MainActivity.class);
        finish();
        startActivity(intent);
    }

    private void initViews() {
        Bundle bundle = getIntent().getExtras();
        headLine = bundle.getString("ArticleHeadLine");
        link = bundle.getString("ArticleLink");

        progressBar=(ProgressBar)findViewById(R.id.progressBar);

        textArticle=(TextView)findViewById(R.id.articleDetailTextview);
        TextView headlineTextView=(TextView)findViewById(R.id.headlineDetailTextView);

        englishword1= (TextView) findViewById(R.id.englishWord1);
        englishword2= (TextView) findViewById(R.id.englishWord2);
        englishword3= (TextView) findViewById(R.id.englishWord3);
        englishword4= (TextView) findViewById(R.id.englishWord4);
        englishword5= (TextView) findViewById(R.id.englishWord5);


        turkishword1=(TextView)findViewById(R.id.turkishWord1);
        turkishword2=(TextView)findViewById(R.id.turkishWord2);
        turkishword3=(TextView)findViewById(R.id.turkishWord3);
        turkishword4=(TextView)findViewById(R.id.turkishWord4);
        turkishword5=(TextView)findViewById(R.id.turkishWord5);


        headlineTextView.setText(headLine);

    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        return new DetailAsyncTaskLoader(this,link);
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {

        progressBar.setVisibility(View.GONE);

        StringAndList snl= (StringAndList) data;
        article=snl.getArticle();
        textArticle.setText(article);


        englishword1.setText((String) snl.getWordMeaningList().keySet().toArray()[0]);
        englishword2.setText((String) snl.getWordMeaningList().keySet().toArray()[1]);
        englishword3.setText((String) snl.getWordMeaningList().keySet().toArray()[2]);
        englishword4.setText((String) snl.getWordMeaningList().keySet().toArray()[3]);
        englishword5.setText((String) snl.getWordMeaningList().keySet().toArray()[4]);

        turkishword1.setText((String) snl.getWordMeaningList().get((String) snl.getWordMeaningList().keySet().toArray()[0]));
        turkishword2.setText((String) snl.getWordMeaningList().get((String) snl.getWordMeaningList().keySet().toArray()[1]));
        turkishword3.setText((String) snl.getWordMeaningList().get((String) snl.getWordMeaningList().keySet().toArray()[2]));
        turkishword4.setText((String) snl.getWordMeaningList().get((String) snl.getWordMeaningList().keySet().toArray()[3]));
        turkishword5.setText((String) snl.getWordMeaningList().get((String) snl.getWordMeaningList().keySet().toArray()[4]));
    }

    @Override
    public void onLoaderReset(Loader loader) {

    }


}
