package com.ok.wiredarticles;

import java.util.HashMap;
import java.util.List;

/**
 * Created by ok on 9.05.2017.
 */

public class StringAndList {

    private String article;
    private HashMap<String,String> wordMeaningList;

    public StringAndList(String article, HashMap<String,String>wordMeaningList) {
        this.article = article;
        this.wordMeaningList=wordMeaningList;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public HashMap<String, String> getWordMeaningList() {
        return wordMeaningList;
    }

    public void setWordMeaningList(HashMap<String, String> wordMeaningList) {
        this.wordMeaningList = wordMeaningList;
    }
}
