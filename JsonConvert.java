package com.ok.wiredarticles;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.select.Elements;

import java.util.ArrayList;

/**
 * Created by ok on 6.05.2017.
 */

public class JsonConvert {

    public static String getArticleHeadline(JSONObject jsonObject) {

        String articleHeadline=null;

        try {
            articleHeadline=jsonObject.getString("headline");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return articleHeadline;
    }

    public static String getArticleDate(JSONObject jsonObject) {

        String articleHeadDate=null;

        try {
            articleHeadDate=jsonObject.getString("datePublished");
            articleHeadDate=articleHeadDate.substring(0,10);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return articleHeadDate;
    }

    public static String getArticleHeadAuthor(JSONObject jsonObject) {

        String articleHeadAuthor=null;

        try {
            articleHeadAuthor=jsonObject.getString("name");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return articleHeadAuthor;
    }

    public static String getArticleLink(JSONObject jsonObject) {

        String articleLink=null;

        try {
            articleLink=jsonObject.getString("url");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return articleLink;
    }


    public static JSONObject StringToJsonObject(String jsonString) throws JSONException {

        JSONObject jsonObj = new JSONObject(jsonString);

        return jsonObj;
    }



}
