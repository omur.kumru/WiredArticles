package com.ok.wiredarticles;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.view.LayoutInflater;
import android.widget.TextView;
import java.util.ArrayList;


/**
 * Created by ok on 6.05.2017.
 */

public class CustomAdapter extends ArrayAdapter<Article> {



    public CustomAdapter(Context context, ArrayList<Article> articles) {
        super(context, 0, articles);
    }




    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Article articles = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_article, parent, false);
        }
        // Lookup view for data population
        TextView articleName = (TextView) convertView.findViewById(R.id.articleHeadlineTextView);
        TextView articleCatagory = (TextView) convertView.findViewById(R.id.articleCatagoryTextView);
        TextView articleDate=(TextView)convertView.findViewById(R.id.articleDateTextView);
        // Populate the data into the template view using the data object

        articleName.setText(articles.getArticleHeadline());
        articleCatagory.setText(articles.getAuthor());
        articleDate.setText(articles.getDate());

        // Return the completed view to render on screen
        return convertView;
    }





}
