package com.ok.wiredarticles;

/**
 * Created by ok on 6.05.2017.
 */

public class Article {

    private String mArticleHeadline;
    private String mAuthor;
    private String mDate;
    private String mLink;

    public Article(String mArticleHeadline, String mAuthor, String mDate, String mLink) {
        this.mArticleHeadline = mArticleHeadline;
        this.mAuthor = mAuthor;
        this.mDate = mDate;
        this.mLink = mLink;
    }

    public String getArticleHeadline() {
        return mArticleHeadline;
    }

    public void setArticleHeadline(String mArticleHeadline) {
        this.mArticleHeadline = mArticleHeadline;
    }

    public String getAuthor() {
        return mAuthor;
    }

    public void setAuthor(String mAuthor) {
        this.mAuthor = mAuthor;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String mDate) {
        this.mDate = mDate;
    }

    public String getLink() {
        return mLink;
    }

    public void setLink(String mLink) {
        this.mLink = mLink;
    }
}
