package com.ok.wiredarticles;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by ok on 9.05.2017.
 */

public class ConnectionCheck {

    public static boolean isNetworkAvailable(Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }
}
