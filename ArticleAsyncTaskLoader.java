package com.ok.wiredarticles;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.text.Html;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by ok on 6.05.2017.
 */

public class ArticleAsyncTaskLoader extends AsyncTaskLoader<ArrayList<Article>> {


    final String URL = "https://www.wired.com";



    String baslik;

    public ArticleAsyncTaskLoader(Context context) {
        super(context);
    }

    @Override
    public ArrayList<Article> loadInBackground() {
        ArrayList<Article> articleArrayList = new ArrayList<>();
        Document doc = null;
        try {
            doc = Jsoup.connect(URL).get();//Site ile bağlantı kurulur. Bu bağlantı doc nesnesine aktarılır.
        } catch (IOException e) {
            e.printStackTrace();
        }


       for (int i = 1; i<=5; i++) {

           String articleHeadline,articleDate,articleAuthor,articleLink=null;
           JSONObject jsonObject=null;
           JSONObject jsonObject1=null;
            String divRequest = "div#p" + i + " ";





           try {
               Elements element = doc.select(divRequest);
               Elements element1= element.select("script[type=\"application/ld+json\"]");
               String jsonElement = element1.first().html();

               jsonObject=JsonConvert.StringToJsonObject(jsonElement);
               jsonObject1=jsonObject.getJSONObject("author");
           } catch (JSONException e) {
               e.printStackTrace();
           }catch (NullPointerException e){

           }

           articleHeadline=JsonConvert.getArticleHeadline(jsonObject);
           articleDate=JsonConvert.getArticleDate(jsonObject);
           articleAuthor=JsonConvert.getArticleHeadAuthor(jsonObject1);
           articleLink=JsonConvert.getArticleLink(jsonObject);

           articleArrayList.add(new Article(articleHeadline,articleAuthor,articleDate,articleLink));

       }
        return articleArrayList;
    }

        @Override
        protected void onStartLoading () {
            forceLoad();
        }



}
