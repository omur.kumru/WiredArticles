package com.ok.wiredarticles;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ok on 9.05.2017.
 */

public class JsonRequest {


    public static JSONObject getJSONObjectFromURL(String urlString) throws IOException, JSONException {

        HttpURLConnection urlConnection = null;


        URL url = new URL(urlString);

        urlConnection = (HttpURLConnection) url.openConnection();

        urlConnection.setRequestMethod("GET");
        urlConnection.setReadTimeout(10000 /* milliseconds */);
        urlConnection.setConnectTimeout(15000 /* milliseconds */);

        urlConnection.setDoOutput(true);

        urlConnection.connect();

        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));

        char[] buffer = new char[1024];

        String jsonString = new String();

        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line + "\n");
        }
        br.close();

        jsonString = sb.toString();

        System.out.println("JSON: " + jsonString);

        return new JSONObject(jsonString);
    }

    public static HashMap<String, String> getJsonMeaning(List<String> fiveWords) {

        String descJson;


        HashMap<String, String> wordsMeaning = new HashMap<>();

        for (String word : fiveWords) {

            String JSON_REQUEST = "https://dictionary.yandex.net/api/v1/dicservice.json/lookup?key=dict.1.1.20170509T181225Z.49d7084fbc5cf883.b7a5630e66b0d995c8fbb85d404a10b392e31eed&lang=en-tr&text=" + word;

            try {
                JSONObject requestJson = JsonRequest.getJSONObjectFromURL(JSON_REQUEST);

                JSONArray wordJsonArray = requestJson.getJSONArray("def");

                JSONObject wordJsonObject = wordJsonArray.getJSONObject(0);

                JSONArray trJsonArray = wordJsonObject.getJSONArray("tr");

                JSONObject trJsonObject1 = trJsonArray.getJSONObject(0);

                String trJsonObject1mean = trJsonObject1.getString("text");

                JSONObject trJsonObject2 = trJsonArray.getJSONObject(1);

                String trJsonObject2mean = trJsonObject2.getString("text");


                descJson = trJsonObject1mean + " , " + trJsonObject2mean;
                wordsMeaning.put(word, descJson);


            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                descJson = "not found";
                wordsMeaning.put(word, descJson);
                e.printStackTrace();
            }

        }


        return wordsMeaning;
    }
}
