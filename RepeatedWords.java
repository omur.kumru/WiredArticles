package com.ok.wiredarticles;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by ok on 8.05.2017.
 */

public class RepeatedWords {



    public static List<String> repeatedWords(String originalText) {

        Set<String> bannedWords=bannedWorldCreator();



        String str = originalText.toLowerCase();

        String newStr = str.replace(","," ").replace("!"," ").replace("."," ").replace("("," ").replace(")"," ").replace("["," ").replace("]"," ").replace("-"," ").replace("?"," ").replace("\""," ");

        String splitStr[]=newStr.split("\\s+");

        Map<String, Integer> wordCount = new HashMap<>();
        for (String word : splitStr) {
            if(word.length()>=2){
            if (wordCount.containsKey(word)) {
                // Map already contains the word key. Just increment it's count by 1


                wordCount.put(word, wordCount.get(word) + 1);
            } else {
                // Map doesn't have mapping for word. Add one with count = 1
                wordCount.put(word, 1);
            }
            }
        }


        Map <String,Integer> sortedCount;
        sortedCount=MapUtil.sortByValue(wordCount);

        sortedCount.keySet().removeAll(bannedWords);


        List<String> list = new ArrayList<String>();


        for ( Map.Entry<String, Integer> eentry: sortedCount.entrySet()) {


            if(list.size()<=4){
                list.add(eentry.getKey());

            }

        }



        return list;


    }
    public static Set<String> bannedWorldCreator (){

        Set<String> bannedWords= new HashSet<>();
        bannedWords.add("a");
        bannedWords.add("an");
        bannedWords.add("for");
        bannedWords.add("if");
        bannedWords.add("to");
        bannedWords.add("in");
        bannedWords.add("and");
        bannedWords.add("of");
        bannedWords.add("the");
        bannedWords.add("that");
        bannedWords.add("you");
        bannedWords.add("me");
        bannedWords.add("him");
        bannedWords.add("she");
        bannedWords.add("he");
        bannedWords.add("your");
        bannedWords.add("I");
        bannedWords.add("at");
        bannedWords.add("is");
        bannedWords.add("it");
        bannedWords.add("with");
        bannedWords.add("or");
        bannedWords.add("have");
        bannedWords.add("has");
        bannedWords.add("had");


        return bannedWords;

    }


}
